clean:
	rm -f foobarqix.exe

convert:
	dos2unix.exe src/*.CBL

build: convert
	cobc -x src/*.CBL

run: build
	./foobarqix.exe